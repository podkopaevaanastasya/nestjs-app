# NestJS app
In this application I'm going to build REST API with NestJS with CRUD operations.

Documentation: https://docs.nestjs.com/.

## Version control system

I use GIT command line locally for this project.

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

I'll write about:
- [ ] Purpose of the project 
- [ ] Project status
- [ ] Installation
- [ ] Usage and visuals
- [ ] Features and roadmap