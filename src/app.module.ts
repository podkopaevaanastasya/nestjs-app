// this file is a main file of application

// @Module - it's a decorator (function that adds metadata to classes)
// The @Module() decorator takes a single object whose properties describe the module (providers, controllers, imports, exports)


import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { BookmarkModule } from './bookmark/bookmark.module';
import { PrismaModule } from './prisma/prisma.module';

@Module({
  imports: [AuthModule, UserModule, BookmarkModule, PrismaModule],
})
export class AppModule {}
